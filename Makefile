.PHONY: build run clean

build:
	docker build -t focs .

run: build
	docker run -p 8888:8888 -v "$(CURDIR)/notebooks:/notebooks" -it focs jupyter notebook --ip=0.0.0.0
